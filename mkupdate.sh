#!/bin/bash
#
# Create an update (*.upd) package for the Mooring Controller. The structure is almost
# identical to an IPKG package file. Some of the code in this script was "borrowed"
# from ipkg-build.
#
if which gnutar 1> /dev/null 2>&1
then
    TAR=gnutar
else
    TAR=tar
fi

export COPYFILE_DISABLE=true

ogargs=""
usage="Usage: $(basename $0) [-o owner] [-g group] directory pkgname"
while getopts "g:ho:" opt
do
    case $opt in
        o ) 
	    owner=$OPTARG
            ogargs="$ogargs --owner=$owner"
            ;;
        g ) 
	    group=$OPTARG
            ogargs="$ogargs --group=$group"
            ;;
        h )     
	    echo $usage 1>&2 
	    ;;
        \? )
	    echo $usage 1>&2
	    ;;
    esac
done

shift $(($OPTIND - 1))

if [ $# -lt 2 ]
then
    echo $usage 1>&2
    exit 1
fi

pkgdir="$1"
pkgname="$PWD/$2"

if [ ! -d "$pkgdir" ]
then
    echo "ERROR: Package directory \"$pkgdir\" not found" 1>&2
    exit 1
fi

if [ ! -d "$pkgdir/CONTROL" ]
then
    echo "ERROR: Package directory has no CONTROL subdirectory" 1>&2
    exit 1
fi

tmpdir="$PWD/updbuild.$$"
mkdir $tmpdir
trap "rm -rf $tmpdir;exit 1" 0 1 2 3 15

set -e

(cd "$pkgdir" && $TAR $ogargs --exclude=CONTROL -czf $tmpdir/files.tar.gz .)
(cd "$pkgdir/CONTROL" && $TAR $ogargs -czf $tmpdir/control.tar.gz .)
(cd $tmpdir && $TAR $ogargs -czf "$pkgname" ./files.tar.gz ./control.tar.gz)

echo "Packaged contents of \"$pkgdir\" into \"$pkgname\""

