#!/bin/bash
#
# Grab a segment of streaming audio from the ALOHA Cabled Observatory (ACO) and
# save to a file.
#
if [ -d /Applications/VLC.app ]
then
    # OSX
    CVLC="/Applications/VLC.app/Contents/MacOS/VLC -I dummy"
else
    # Linux/Unix
    CVLC=cvlc
fi

if [ "$(whoami)" = "mooring" ]
then
    DATADIR="$HOME/hot-3/unpacked/AUDIO/$(date -u +'%Y/%m/%d')"
else
    DATADIR=$HOME/data/HOT
fi

duration="$1"
[ -z "$duration" ] && duration=180

mkdir -p $DATADIR
tstamp=$(date -u +'%Y%m%d_%H%M%S')
datafile="aco-$tstamp.mp3"

stream="#duplicate{dst=std{access=file,mux=raw,dst=$DATADIR/.incoming.mp3}}"
$CVLC --run-time=$duration http://aco.soest.hawaii.edu:8000 --sout "$stream" vlc://quit
mv $DATADIR/.incoming.mp3 $DATADIR/$datafile