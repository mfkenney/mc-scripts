#!/bin/sh
#
# Perform initial OS setup for a Mooring Controller board. Before running this script, the
# custom kernel and root filesystem must be installed.
#
PATH=/bin:/sbin:/usr/bin:/usr/sbin
export PATH

EXTRA_PKGS="libssl0.9.7 libcrypto0.9.7 db libsqlite3-0 libthread-db1 dialog coreutils"
PYTHON_PKGS="python-core libpython2.5-1.0 python-curses python-xml python-dbus python-pymc python-pyyaml python-subprocess python-pyserial python-shell python-simplejson"
REMOVE="linux-libc-headers-dev libc6-dev libgcc-s-dev dbus-dev libexpat-dev glibc-extra-nss"

if grep -q "/media/cf" /proc/mounts
then 
    :
else
    echo "CF card is not mounted!"
    exit 1
fi

fstype=`grep "/media/cf" /proc/mounts|head -n 1|cut -f3 -d' '`

if test "$fstype" = "ext2"
then
    :
else
    echo "CF card must have ext2 filesystem"
    echo "reformat with: 'mke2fs -m0 /dev/hda1'"
    exit 1
fi

# Create a custom destination for the Python packages as they are too large for the
# root filesystem.
PKG_ROOT="/media/cf/root"
mkdir $PKG_ROOT
echo "dest cf $PKG_ROOT" >> /etc/ipkg.conf
ipkg update

ipkg install $EXTRA_PKGS
ipkg -d cf install $PYTHON_PKGS
# python-dbus pulls in a bunch of development packages that we don't need.
ipkg -force-depends remove $REMOVE

ipkg-link mount $PKG_ROOT

# Install a modified version of the D-bus config file
DBUSCFG=/etc/dbus-1/system.conf
cp $DBUSCFG ${DBUSCFG}.orig
cat<<EOF > $DBUSCFG
<!DOCTYPE busconfig PUBLIC "-//freedesktop//DTD D-Bus Bus Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
<busconfig>

  <!-- Our well-known bus type, do not change this -->
  <type>system</type>

  <!-- Run as special user -->
  <user>root</user>

  <!-- Fork into daemon mode -->
  <fork/>

  <servicedir>/usr/share/dbus-1/services</servicedir>

  <!-- Write a pid file -->
  <pidfile>/var/run/dbus/pid</pidfile>

  <!-- Only allow socket-credentials-based authentication -->
  <auth>EXTERNAL</auth>

  <!-- Only listen on a local socket. (abstract=/path/to/socket 
       means use abstract namespace, don't really create filesystem 
       file; only Linux supports this. Use path=/whatever on other 
       systems.) -->
  <listen>unix:path=/var/run/dbus/system_bus_socket</listen>

  <policy context="default">
    <!-- Deny everything then punch holes -->
    <deny send_interface="*"/>
    <deny receive_interface="*"/>
    <deny own="*"/>
    <!-- But allow all users to connect -->
    <allow user="*"/>
    <!-- Allow anyone to talk to the message bus -->
    <!-- FIXME I think currently these allow rules are always implicit 
         even if they aren't in here -->
    <allow send_destination="org.freedesktop.DBus"/>
    <allow receive_sender="org.freedesktop.DBus"/>
    <!-- valid replies are always allowed -->
    <allow send_requested_reply="true"/>
    <allow receive_requested_reply="true"/>
  </policy>

  <!-- Config files are placed here that among other things, punch 
       holes in the above policy for specific services. -->
  <includedir>system.d</includedir>

  <!-- This is included last so local configuration can override what's 
       in this standard file -->
  <include ignore_missing="yes">system-local.conf</include>

  <include if_selinux_enabled="yes" selinux_root_relative="yes">contexts/dbus_contexts</include>

</busconfig>
EOF
