#!/bin/bash
#
# Extract NEMO/ChaBa file transfer information from an IKSD log file. The output
# is written to standard out and contains the following three columns of ascii text:
#
#  timestamp (seconds since the epoch)
#  transfer time (seconds)
#  file size (bytes)
#
# If the --plot option is used, the epoch is 1/1/2001 UTC for compatibility with the
# OSX Plot program, otherwise it is the standard Unix epoch, 1/1/1970 UTC.
#

if which gdate 1> /dev/null 2>&1
then
    DATE=gdate
else
    DATE=date
fi

# Freewave logins originate from this domain
HOST="centurytel.net"
# Freewave logins use this user account
USER="mooring"

if [ "$1" = "--plot" ]
then
    epoch=$($DATE -d "2001-01-01 00:00:00Z" +%s)
    shift
    echo "Output will be compatible with OSX Plot" 1>&2
else
    epoch=0
fi

infile="$1"
if [ -z "$infile" ]
then
    echo "Usage: $(basename $0) [--plot] iksdlog" 1>&2
    exit 1
fi

declare -a line
grep "$HOST" "$infile" | grep "$USER" |\
while read -a line
do
    ref="${line[@]:0:5}"
    t=$($DATE -d "$ref" +%s)
    echo "$((t - epoch)) ${line[5]} ${line[7]}"
done
