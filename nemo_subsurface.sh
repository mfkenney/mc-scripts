#!/bin/bash
#
# This script is called whenever a new file arrives the the Subsurface Mooring "inbox".
#
# Archive data from the NEMO Subsurface Mooring into a directory tree as follows:
#
# $NEMO_ROOT/
# |
# |- SBE/ (Seabird CTD data)
# |- ADP/ (ADCP data)
# |- MMP/
# |
# |- CTD/ (MMP CTD files)
# |- ACM/ (MMP ACM files)
# |- ENG/ (MMP engineering files)
# |- SUNA/ (MMP SUNA files)
# |- byProfile/
# |
# |- 0000001/ (links to files from profile 1)
# ...
# |- NNNNNNN/ (links to files from profile N)
#
export PATH=$HOME/.venvs/mooring/bin:$PATH

: ${NEMO_ROOT=$HOME/NEMO_SUBSURFACE}

store_mmp ()
{
    pathname=$1
    filename=$2
    dev=$3
    pnum=$(cut -c 2-8 <<< "$filename")
    cp "$pathname" $NEMO_ROOT/MMP/$dev
    rm -f "$pathname"
    chmod a+r $NEMO_ROOT/MMP/$dev/$filename
    pdir="$NEMO_ROOT/MMP/byProfile/$pnum"
    mkdir -p $pdir
    ln -s $NEMO_ROOT/MMP/$dev/$filename $pdir/$filename
}

if [[ -z "$1" ]]; then
    echo "Usage: $(basename $0) filename"
    exit 1
fi

[[ -s "$HOME/.nemo_root" ]] && NEMO_ROOT="$(<$HOME/.nemo_root)"

mkdir -p $NEMO_ROOT/{SBE,ADP,MMP/{CTD,ACM,ENG,SUNA,byProfile}}

pathname="$1"
filename=$(basename $pathname)
this_dir=$(dirname "$1")

case "$filename" in
    *.sbe)
        cp "$pathname" $NEMO_ROOT/SBE
        rm -f "$pathname"
        chmod a+r $NEMO_ROOT/SBE/$filename
        ;;
    *.adp)
        cp "$pathname" $NEMO_ROOT/ADP
        rm -f "$pathname"
        chmod a+r $NEMO_ROOT/ADP/$filename
        ;;
    c*.dat)
        csvfile="${filename%.dat}.csv"
        ctdo2csv "$pathname" "$this_dir/$csvfile"
        store_mmp "$pathname" "$filename" CTD
        ;;
    c*.csv)
        store_mmp "$pathname" "$filename" CTD
        ;;
    a*.dat)
        csvfile="${filename%.dat}.csv"
        acm2csv "$pathname" "$this_dir/$csvfile"
        store_mmp "$pathname" "$filename" ACM
        ;;
    a*.csv)
        store_mmp "$pathname" "$filename" ACM
        ;;
    e*.dat)
        store_mmp "$pathname" "$filename" ENG
        ;;
    s*.dat)
        store_mmp "$pathname" "$filename" SUNA
        ;;
    *.gz)
        # Gzip compressed MMP files
        uc_filename=$(echo ${filename%.gz}.dat|tr "[A-Z]" "[a-z]")
        zcat "$pathname" > "$this_dir/$uc_filename"
        rm -f "$pathname"
        ;;
esac
