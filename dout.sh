#!/bin/sh
#
# $Id: dout.sh,v 84927e3a17ad 2007/09/28 22:03:30 mikek $
#
# Use the /proc GPIO interface to set/clear output lines. Requires
# that the proc-gpio module be loaded.
#
#   modprobe proc-gpio
#

if [ $# -ne 2 ]
then
    echo "Usage: $0 <line> <set|clear>"
    exit 1
fi

file=`printf "/proc/gpio/GPIO%02d" $1`
if [ ! -e "$file" ]
then
    echo "Invalid GPIO line number, $1"
    exit 1
fi

echo "GPIO out $2" > $file
