#!/bin/sh
#
# Wakeup the Seascan clock.
#

gpio ()
{
    line=$1
    state=$2

    echo "GPIO out $state" > /proc/gpio/GPIO${line}
}

# GPIO clock wakeup line
WAKEUP=76
# Active state
ACTIVE=high
# Number of pulses to send
PULSES=3
# Pulse length in microseconds
PULSELEN=10000

if [ "$ACTIVE" = "low" ]
then
    assert="clear"
    deassert="set"
else
    assert="set"
    deassert="clear"
fi

echo -n "Pulsing wakeup line "
i=0
while [ $i -lt $PULSES ]
do
    echo -n "."
    gpio $WAKEUP $assert
    usleep $PULSELEN
    gpio $WAKEUP $deassert
    i=`expr $i + 1`
done
echo ""

