#!/bin/bash
#
# Build a Mooring Controller configuration package.
#
PATH=/bin:/sbin:/usr/bin:/usr/local/bin
export PATH

if [ $# -lt 1 ]
then
    echo "Usage: $(basename $0) package_dir"
    exit 1
fi

dir=$1

# Incorporate the current date/time into the version string.
version="0.$(date +%Y%m%d).$(date +%H%M)-r1"
sed -i -e "s/Version:.*$/Version: $version/" $dir/CONTROL/control

ipkg-build -o root -g root $dir
