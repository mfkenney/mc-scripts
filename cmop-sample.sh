#!/bin/sh
#
# Sample the CMOP sensors.
#
PATH=/bin:/usr/bin:/sbin:/usr/sbin
export PATH

set -e

# Timeout must be longer than the ADP averaging interval (120 seconds)
SAMPLE_TIMEOUT=130
SWITCHES="ext3 ext1"
PORTS="/dev/ttyUSB2 /dev/ttyUSB0"

# Enable the multiport USB/serial device
on.py usb
# Wait for device to be enabled
while true
do
    test -e /dev/ttyUSB3 && break
    sleep 1
done

# Work around the mysterious sensor startup problem. Power-on the sensors,
# send a couple of carriage-returns and power them off.
on.py $SWITCHES
for p in $PORTS
do
    echo "" > $p
    sleep 1
    echo "" > $p
    sleep 1
done
off.py $SWITCHES

# Do the actual sampling.
sleep 2
sensor-sample -t $SAMPLE_TIMEOUT adp/1 ctd/1

# Disable the USB/serial device
off.py usb
