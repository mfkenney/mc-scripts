#!/bin/bash
#
# Create the initial directory structure for an update package relative to
# a specified git commit.
#
if which gnutar 1> /dev/null 2>&1
then
    TAR=gnutar
else
    TAR=tar
fi

if [ $# -lt 2 ]
then
    echo "Usage: $(basename $0) basedir commit-id"
    exit 1
fi

basedir="$1"
commit="$2"

export COPYFILE_DISABLE=true

mkdir -p "$basedir"
$TAR -c -f - $(git diff --name-only "$commit") |\
  $TAR -x -v -C "$basedir" -f -
mkdir -p "$basedir/CONTROL"
echo -e "#!/bin/sh\nexit 0" > "$basedir/CONTROL/preinst"
chmod a+x "$basedir/CONTROL/preinst"
echo -e "#!/bin/sh\nexit 0" > "$basedir/CONTROL/postinst"
chmod a+x "$basedir/CONTROL/postinst"
