#!/bin/bash
#
# Create a monthly archive of a Mooring data directory. This script
# is run by cron on the first day of the month.
#

basedir="$1"
[ -z "$basedir" ] && exit 1
cd "$basedir" || exit 1

dest="./ARCHIVE/$(date -d '-1 day' +%Y-%m)"
mkdir -p "$dest"
[ -d "$dest" ] || exit 1

for name in *
do
    case "$name" in
	RAW)
	    ;;
	ARCHIVE)
	    ;;
	*)
	    mv "$name" "$dest"
	    ;;
    esac
done
